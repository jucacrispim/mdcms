# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.template import RequestContext

class BaseHTMLView(object):
    controller_class = None
    show_template = None
    list_template = None

    def __init__(self, request):
        self.controller = self.controller_class(request)


    def show(self, slug):
        context = {}
        context['content'] = self.controller.get(slug)
        extra = self.get_show_extra_context(context['content'])
        context.update(extra)
        return self.render_template(self.show_template, **context)

    def list(self, first, last, exclude_uuids=None, **kwargs):
        """ List films using ``first`` and ``last`` as slice index
        """
        clist = self.controller.get_list(first, last, exclude_uuids, **kwargs)
        context = {}
        context['contents'] = clist
        extra = self.get_list_extra_context(context)
        context.update(extra)
        return self.render_template(self.list_template, **context)

    def get_list_extra_context(self, context):
        """ Extra context to list content. Overide it in
        your own class.
        """
        return {}

    def get_show_extra_context(self, content):
        """ Extra context to show content. Overide it in
        your own class.
        """
        return {}

    def render_template(self, template_name, **context):
        """ Renders a template using context
        :param template_name: name for the template
        :param context: dictionary containing the context for the template
        """
        return render(
            self.controller.request, template_name, context,
            context_instance=RequestContext(self.controller.request))
