# -*- coding: utf-8 -*-

from mdcms.exceptions import ImproperlyConfiguredController


class BaseModelController(object):
    """ Base controller for handling models.
    You MUST define ``model`` in your class scope.
    """

    model = None

    def __init__(self, request):
        self.request = request
        if self.model is None:
            msg = 'The controller model can not be None'
            raise ImproperlyConfiguredController(msg)

    def get(self, slug):
        """ Return a content based on its slug
        """
        content = self.model.objects.get(slug=slug)
        return content

    def get_list(self, first=0, last=8, exclude_uuids=None, order_by=None,
                 **kwargs):
        """ Return a list of contents.
        :param fisrt: index of fist element
        :param last: index of the last element (excluding itself)
        :param exclude_ids: List of film ids to be excluded.
        :param kwargs: Params to filter
        """
        contents = self.model.objects.filter(**kwargs)

        if order_by:
            contents = contents.order_by(order_by)

        if exclude_uuids is None:
            exclude_uuids = []

        if exclude_uuids:
            contents = contents.exclude(uuid__in=exclude_uuids)

        return contents[first:last]
