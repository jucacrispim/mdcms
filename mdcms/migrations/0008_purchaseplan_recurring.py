# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdcms', '0007_auto_20150319_1616'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaseplan',
            name='recurring',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
