# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdcms', '0006_auto_20150219_1420'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaseplan',
            name='dollar_value',
            field=models.DecimalField(default=0.0, verbose_name='Value in Dollars', max_digits=6, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='purchaseplan',
            name='value',
            field=models.DecimalField(verbose_name='Value in Reais', max_digits=6, decimal_places=2),
            preserve_default=True,
        ),
    ]
