# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdcms', '0003_auto_20150128_1925'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='purchaseplan',
            options={'verbose_name': 'Purchase plan', 'verbose_name_plural': 'Purchase plans'},
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='description_en',
            field=models.CharField(max_length=256, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='description_es',
            field=models.CharField(max_length=256, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='description_fr',
            field=models.CharField(max_length=256, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='description_it',
            field=models.CharField(max_length=256, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='description_pt_BR',
            field=models.CharField(max_length=256, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='name_en',
            field=models.CharField(max_length=32, null=True, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='name_es',
            field=models.CharField(max_length=32, null=True, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='name_fr',
            field=models.CharField(max_length=32, null=True, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='name_it',
            field=models.CharField(max_length=32, null=True, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='purchaseplan',
            name='name_pt_BR',
            field=models.CharField(max_length=32, null=True, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='purchaseplan',
            name='description',
            field=models.CharField(max_length=256, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='purchaseplan',
            name='name',
            field=models.CharField(max_length=32, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='purchaseplan',
            name='value',
            field=models.DecimalField(verbose_name='Value', max_digits=6, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='user',
            name='confirmed',
            field=models.BooleanField(default=0, verbose_name='Confirmed'),
            preserve_default=True,
        ),
    ]
