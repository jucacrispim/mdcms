# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdcms', '0003_auto_20150122_2339'),
    ]

    operations = [
        migrations.RenameField(
            model_name='purchaseplan',
            old_name='description_pt_BR',
            new_name='description_pt_br',
        ),
        migrations.RenameField(
            model_name='purchaseplan',
            old_name='name_pt_BR',
            new_name='name_pt_br',
        ),
    ]
