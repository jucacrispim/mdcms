# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mdcms', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchaseplan',
            name='description',
            field=models.CharField(max_length=256, null=True, verbose_name='Descri\xe7\xe3o', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='purchaseplan',
            name='name',
            field=models.CharField(max_length=32, verbose_name='Nome'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='purchaseplan',
            name='value',
            field=models.DecimalField(verbose_name='Valor', max_digits=6, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='user',
            name='confirmed',
            field=models.BooleanField(default=0, verbose_name='Confirmado'),
            preserve_default=True,
        ),
    ]
