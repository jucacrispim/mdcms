# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Login',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(max_length=64)),
                ('ip', models.CharField(max_length=15)),
                ('logout', models.BooleanField(default=0)),
                ('create', models.DateTimeField()),
                ('expire', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PurchasePlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, verbose_name='Name')),
                ('description', models.CharField(max_length=256, null=True, verbose_name='Description', blank=True)),
                ('trial', models.BooleanField(default=False, verbose_name='Trial')),
                ('trial_period', models.IntegerField(default=0, verbose_name='Trial period')),
                ('trial_period_type', models.CharField(default=b'M', max_length=1, verbose_name='Trial period type', choices=[(b'D', 'Day'), (b'M', 'Month'), (b'Y', 'Year')])),
                ('trial_value', models.DecimalField(default=0.0, verbose_name='Trial value', max_digits=6, decimal_places=2)),
                ('period', models.IntegerField(verbose_name='Period')),
                ('period_type', models.CharField(default=b'M', max_length=1, verbose_name='Period type', choices=[(b'D', 'Day'), (b'M', 'Month'), (b'Y', 'Year')])),
                ('value', models.DecimalField(verbose_name='Value', max_digits=6, decimal_places=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('confirmed', models.BooleanField(default=0, verbose_name='Confirmed')),
                ('token', models.CharField(max_length=64)),
                ('payment_ok', models.BooleanField(default=False, verbose_name='Payment ok')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
        ),
        migrations.AddField(
            model_name='login',
            name='user',
            field=models.ForeignKey(to='mdcms.User'),
            preserve_default=True,
        ),
    ]
