# -*- coding: utf-8 -*-

from django.db import models
from mdcms.exceptions import BaseAbstractClassNotFound
from mdcms.utils import classproperty, fqualname

class QuerysetUnionMixin(object):
    """ Mixin to be used on subclasses of abstract classes that want
    to query over all subclasses from its model super class.
    """

    @classproperty
    def objects(cls):
        """ Method to keep the same interface as non abstract models
        while returning a union of the queryset from the subclasses.
        """

        baseclass = cls.get_base_model_abstract_class()
        subclasses = [c for c in baseclass.__subclasses__() if
                      fqualname(c) != fqualname(cls)]

        if not subclasses:
            return models.Manager().none()

        qs = subclasses.pop(0).objects.filter()

        for s in subclasses:
            qs = qs | s.objects.filter()
        return qs

    @classmethod
    def get_base_model_abstract_class(cls):
        for c in cls.__bases__:
            if hasattr(c, '_meta') and c._meta.abstract:
                return c

        msg = 'Base abstract class for %s not found' % cls.__name__
        raise BaseAbstractClassNotFound(msg)
