# -*- coding: utf-8 -*-

from django.forms.widgets import ClearableFileInput


class ImgClearableFileInput(ClearableFileInput):
    """ Widget that displays the image on admin
    """

    url_markup_template = '<img src="{0}"/>'
