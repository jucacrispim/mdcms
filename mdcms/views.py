# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _
from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import render
from django.core import urlresolvers
from mdcms import controllers
from mdcms.base.views import BaseHTMLView


class PurchasePlanHTMLView(BaseHTMLView):
    controller_class = controllers.PurchasePlanController
    show_template = 'mdcms/show_purchase_plan.html'
    list_template = 'mdcms/list_purchase_plans.html'


def list_plans(request):
    userctrl = controllers.UserController(request)
    if not userctrl.is_logged():
        raise Exception('you must be logged')

    view = PurchasePlanHTMLView(request)
    first = 0
    last = 10
    return view.list(first, last)

def buy(request):
    userctrl = controllers.UserController(request)
    if not userctrl.is_logged():
        raise Exception('you must be logged')
    controller = controllers.PurchasePlanController(request)
    user = userctrl._get_logged_user()
    return controller.buy(user)

def show_change_password_by_url_form(request, change_password_token):
    """ Returns a form to change password requested by a url
    (url is usually sent by email)
    """

    controller = controllers.UserController(request)
    try:
        user = controller.get_user(change_password_token=change_password_token)
    except:
        return HttpResponseServerError(_('Invalid token'))

    token = change_password_token

    template = 'mdcms/change_password_by_email_form.html'
    action = urlresolvers.reverse('change-password')
    context = {'token': token, 'action': action}
    return render(request, template, context)


def change_password(request):
    controller = controllers.UserController(request)
    return controller.change_password(sendmail=True)

def change_email(request):
    controller = controllers.UserController(request)
    return controller.change_email(sendmail=True)

def request_change_password_url(request):
    controller = controllers.UserController(request)
    controller.request_change_password_url()
    return HttpResponse(_('Email sent'))

def request_change_password_token(request):
    controller = controllers.UserController(request)
    return controller.request_change_password_token()
