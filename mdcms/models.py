# -*- coding: utf-8 -*-

from uuid import uuid4
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User as UserBase
from django_merceariadigital.models import Order


class PurchasePlan(models.Model):
    """ A purchase plan for content access
    """

    PERIOD_TYPES = (('D', _('Day')),('M', _('Month')), ('Y', _('Year')))

    name = models.CharField(_('Name'), max_length=32)
    description = models.CharField(_('Description'), max_length=256,
                                   null=True, blank=True)
    trial = models.BooleanField(_('Trial'), default=False)
    trial_period = models.IntegerField(_('Trial period'), default=0)
    trial_period_type = models.CharField(_('Trial period type'), max_length=1,
                                   choices=PERIOD_TYPES, default='M')
    trial_value = models.DecimalField(_('Trial value'), max_digits=6,
                                      decimal_places=2, default=0.0)

    period = models.IntegerField(_('Period'))
    period_type = models.CharField(_('Period type'), max_length=1,
                                   choices=PERIOD_TYPES, default='M')
    value = models.DecimalField(_('Value in Reais'), max_digits=6,
                                decimal_places=2)
    dollar_value = models.DecimalField(_('Value in Dollars'), max_digits=6,
                                       decimal_places=2)
    recurring = models.BooleanField(default=False)


    class Meta:
        verbose_name = _('Purchase plan')
        verbose_name_plural = _('Purchase plans')

    def __unicode__(self):  # pragma: no cover
        return self.name or ''

    def get_end_date(self, init_date):
        """ Returns the end date for a given initial date
        :param init_date: datetime object
        """

        if self.period_type == 'M':
            days = 30 * self.period

        elif self.period_type == 'D':
            days = 1 * self.period

        elif self.period_type == 'Y':
            days = 365 * self.period

        end_date = init_date + timezone.timedelta(days=days)
        return end_date


class User(UserBase):
    """ Extension of django's User to handle payment info
    """
    confirmed = models.BooleanField(_('Confirmed'), default=0)
    token = models.CharField(max_length=64)
    payment_ok = models.BooleanField(_('Payment ok'), default=False)
    change_password_token = models.CharField(max_length=64,
                                             null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = str(uuid4())

        # wired! why django is not raising here? what am I doing wrong?
        for f in self.__class__.REQUIRED_FIELDS:
            if not getattr(self, f):
                raise Exception('%s is required' % f)

        super(User, self).save(*args, **kwargs)

    def __unicode__(self):  # pragma: no cover
        return self.email or ''

    @property
    def can_watch_videos(self):
        """ Checks if an user can watch videos
        """
        return self.payment_ok

    @property
    def orders(self):
        return Order.objects.filter(user_token=self.token)

    def get_plan(self):
        now = timezone.now()
        plan = None
        try:
            plan = self.orders.filter(end_date__gte=now).order_by('-id')[0]
        except IndexError:
            pass

        delta = plan.end_date - plan.init_date
        if delta.days >= 365:
            ptpe = _('Yearly')
        elif delta.days >= 30:
            ptype = _('Monthly')
        else:
            ptype = _('Daily')

        return '%s - %s' % (ptype, plan.value)

    def generate_change_password_token(self):
        token = str(uuid4())
        self.change_password_token = token
        self.save()
        return token

    def change_password(self, change_password_token, new_password):
        if self.change_password_token != change_password_token:
            raise Exception(_('Invalid change_password_token'))

        self.set_password(new_password)
        self.change_password_token = None
        self.save()


class Login (models.Model):
    token = models.CharField(max_length=64)
    user = models.ForeignKey(User)
    ip = models.CharField(max_length=15)
    logout = models.BooleanField(default=0)
    create = models.DateTimeField()
    expire = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = str(uuid4())

        if not self.create:
            self.create = timezone.now()
            self.expire =  self.create + timezone.timedelta(days=99)

        super(Login, self).save(*args, **kwargs)

    @classmethod
    def is_valid(cls, token):
        """ Check if a token is valid
        """
        try:
            login = cls.objects.get(token=token)
        except:
            return False

        now = timezone.now()
        if login.expire < now:
            login.delete()
            return False

        return login


    def __unicode__(self):  # pragma: no cover
        return self.token


class Country(models.Model):
    name = models.CharField(max_length=64)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __unicode__(self):  # pragma: no cover
        return self.name or ''
