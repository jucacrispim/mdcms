# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext as _
from mdcms.models import User, PurchasePlan, Country


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'payment_ok',
                    'token',)
    fieldsets = (
        (None, {'fields': ('username', 'confirmed', 'payment_ok', 'token',
                           'last_login',)}),)

    search_fields = ('username', 'email', 'user_token')


class PurchasePlanAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name_pt_br', 'description_pt_br',)}),

        (_('Translations'), {
            'classes': ('collapse', ),
            'fields': ('name_en', 'description_en',
                       'name_es', 'description_es',
                       'name_fr', 'description_fr',
                       'name_it', 'description_it')}),

        (_('Purchase plan info'), {
            'fields': ('trial', 'trial_period', 'trial_period_type',
                       'trial_value', 'period', 'period_type', 'value',
                       'dollar_value', 'recurring')},)
    )


class CountryAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': ('name_pt_br',)}),

        (_('Translations'), {
            'classes': ('collapse', ),
            'fields': ('name_en', 'name_es', 'name_fr', 'name_it',)}),
    )


admin.site.register(User, UserAdmin)
admin.site.register(PurchasePlan, PurchasePlanAdmin)
admin.site.register(Country, CountryAdmin)
