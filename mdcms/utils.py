# -*- coding: utf-8 -*-


class classproperty(property):
    def __get__(self, instance, owner):

        return classmethod(self.fget).__get__(instance, owner)()


def fqualname(cls):
    """ Returns the full qualified name for a given class
    """
    return '%s.%s' % (cls.__module__, cls.__name__)
