# -*- coding: utf-8 -*-

class BaseAbstractClassNotFound(Exception):
    pass


class ImproperlyConfiguredController(Exception):
    pass


class UnknownCtypeForView(Exception):
    pass
