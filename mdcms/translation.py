# -*- coding: utf-8 -*-

from modeltranslation.translator import translator, TranslationOptions
from mdcms import models


class PurchasePlanTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)


class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)


translator.register(models.PurchasePlan, PurchasePlanTranslationOptions)
translator.register(models.Country, CountryTranslationOptions)
