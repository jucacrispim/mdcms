# -*- coding: utf-8 -*-

from modeltranslation.translator import translator, TranslationOptions
from mdcms.dynamicpages import models


class MenuTranslationOptions(TranslationOptions):
    fields = ('title',)


class MostSearchedTranslationOptions(TranslationOptions):
    fields = ('title',)


translator.register(models.Menu, MenuTranslationOptions)
translator.register(models.MostSearched, MostSearchedTranslationOptions)
