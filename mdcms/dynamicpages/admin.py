# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext as _
from mdcms.dynamicpages.models import (MainHighlight, Menu, MostSearched,
                                       ContentType)

class MenuAdmin(admin.ModelAdmin):
    list_display = ('title', 'link', 'order',)
    list_display_links = ('title', 'link', 'order',)
    ordering = ('order',)

    fieldsets = (
        (None, {'fields': ('title_pt_br',)}),

        (_('Translations'), {
            'classes': ('collapse',),
            'fields': ('title_en', 'title_es', 'title_fr',
                       'title_it')}),

        (_('General info'), {
            'fields': ('link', 'order')})

    )


class MostSearchedAdmin(admin.ModelAdmin):
    list_display = ('title', 'link', 'order',)
    list_display_links = ('title', 'link', 'order',)
    ordering = ('order',)

    fieldsets = (
        (None, {'fields': ('title_pt_br',)}),

        (_('Translations'), {
            'classes': ('collapse',),
            'fields': ('title_en', 'title_es', 'title_fr',
                       'title_it')}),

        (_('General info'), {
            'fields': ('link', 'order')})

    )


class MainHighlightAdmin(admin.ModelAdmin):
    list_display = ('admin_thumbnail',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content':
            kwargs['queryset'] = ContentType.objects.filter(
                model='film')

        ret = super(MainHighlightAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs)
        return ret

admin.site.register(Menu, MenuAdmin)
admin.site.register(MainHighlight, MainHighlightAdmin)
admin.site.register(MostSearched, MostSearchedAdmin)
