# -*- coding: utf-8 -*-

from mdcms.dynamicpages.models import Menu

def menu_context_processor(request):
    menus = Menu.objects.all()
    return {'menus': menus}
