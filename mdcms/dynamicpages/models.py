# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from django.contrib.contenttypes.models import ContentType


# shitty stuff. refactor needed!

class Menu(models.Model):
    title = models.CharField(_('Title'), max_length=32)
    link = models.CharField(_('Link'), max_length=32)
    order = models.IntegerField(_('Order'))

    def __unicode__(self):
        return self.title


class MostSearched(models.Model):
    title = models.CharField(_('Title'), max_length=32)
    link = models.CharField(_('Link'), max_length=32)
    order = models.IntegerField(_('Order'))

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Most Searched')
        verbose_name_plural = _('Most Searched')


class MainHighlight(models.Model):
    content = models.ForeignKey(ContentType)
    object_pk = models.CharField(max_length=32)
    picture = models.ImageField(_('Picture') + ' - 1100x452',
                                upload_to=settings.UPLOAD_PATH)

    class Meta:
        verbose_name = _('Main Highlight')
        verbose_name_plural = _('Main Highlight')

    def admin_thumbnail(self):
        return u'<img src="%s" width="100" />' % (self.picture.url)

    @property
    def content_object(self):
        return self.content.get_object_for_this_type(id=self.object_pk)

    admin_thumbnail.short_description = _('Thumbnail')
    admin_thumbnail.allow_tags = True
