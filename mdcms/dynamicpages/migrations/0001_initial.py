# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainHighlight',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_pk', models.CharField(max_length=32)),
                ('picture', models.ImageField(upload_to=b'static/uploads/%Y/%m/%d/', verbose_name='Picture - 1100x452')),
                ('content', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'Main Highlight',
                'verbose_name_plural': 'Main Highlight',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=32, verbose_name='Title')),
                ('title_pt_br', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_en', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_it', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_fr', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_es', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('link', models.CharField(max_length=32, verbose_name='Link')),
                ('order', models.IntegerField(verbose_name='Order')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MostSearched',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=32, verbose_name='Title')),
                ('title_pt_br', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_en', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_it', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_fr', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('title_es', models.CharField(max_length=32, null=True, verbose_name='Title')),
                ('link', models.CharField(max_length=32, verbose_name='Link')),
                ('order', models.IntegerField(verbose_name='Order')),
            ],
            options={
                'verbose_name': 'Most Searched',
                'verbose_name_plural': 'Most Searched',
            },
            bases=(models.Model,),
        ),
    ]
