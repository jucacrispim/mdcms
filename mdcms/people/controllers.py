# -*- coding: utf-8 -*-

from mdcms.controllers import BaseModelController
from mdcms.people.models import Person, Actor


class PersonController(BaseModelController):
    model = Person


class ActorController(BaseModelController):
    model = Actor
