# -*- coding: utf-8 -*-

import importlib
from django.conf import settings
from django.shortcuts import render
from django.template import RequestContext
from django.views.decorators.cache import cache_page
from mdcms.exceptions import UnknownCtypeForView
from mdcms.people.controllers import ActorController

# shitty copy/paste from content. must refactor it!
def get_html_view(ctype):
    """ Returns a class view for ctype
    :param ctype: Type of content
    """

    if ctype == 'actor':
        default_view = 'mdcms.people.views.ActorHTMLView'
        setttings_var = 'ACTOR_HTML_VIEW'
    else:
        raise UnknownCtypeForView(_('Unknown ctype: %s' % ctype))

    try:
        html_view = getattr(settings, setttings_var)
    except AttributeError:
        html_view = default_view

    mod, cls = html_view.rsplit('.', 1)
    mod = importlib.import_module(mod)
    cls = getattr(mod, cls)
    return cls


class ActorHTMLView(object):
    def __init__(self, request):
        self.controller = ActorController(request)
        self.show_template = 'mdcms/people/show_actor.html'
        self.list_template = 'mdcms/people/list_actors.html'

    def show(self, slug):
        context = {}
        context['content'] = self.controller.get(slug)
        extra = self.get_show_extra_context(context['content'])
        context.update(extra)
        return self.render_template(self.show_template, **context)

    def list(self, first, last, exclude_uuids=None, **kwargs):
        """ List films using ``first`` and ``last`` as slice index
        """
        clist = self.controller.get_list(first, last, exclude_uuids, **kwargs)
        context = {}
        context['contents'] = clist
        extra = self.get_list_extra_context(context)
        context.update(extra)
        return self.render_template(self.list_template, **context)

    def get_list_extra_context(self, context):
        """ Extra context to list content. Overide it in
        your own class.
        """
        return {}


    def get_show_extra_context(self, content):
        """ Extra context to show content. Overide it in
        your own class.
        """
        return {}

    def render_template(self, template_name, **context):
        """ Renders a template using context
        :param template_name: name for the template
        :param context: dictionary containing the context for the template
        """
        return render(
            self.controller.request, template_name, context,
            context_instance=RequestContext(self.controller.request))



@cache_page(settings.CACHE_TIME)
def show_actor(request, slug):
    view_class = get_html_view('actor')
    html_view = view_class(request)
    return html_view.show(slug)


@cache_page(settings.CACHE_TIME)
def list_actor(request, first, last):
    exclude_uuids = request.GET.getlist('euuid')
    view_class = get_html_view('actor')
    html_view = view_class(request)
    return html_view.list(first, last, exclude_uuids)
