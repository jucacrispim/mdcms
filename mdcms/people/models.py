# -*- coding: utf-8 -*-

from django.conf import settings
from django.db import models
from django.core import urlresolvers
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import ugettext as _
from mdcms.mixins import QuerysetUnionMixin
from mdcms.models import Country


class BasePerson(models.Model):
    """ Base abastract model for any kind of person related to content.
    """

    name = models.CharField(_('Name'), max_length=64)
    image = models.ImageField(_('Image'), upload_to=settings.UPLOAD_PATH,
                              null=True, blank=True)
    slug = models.CharField(max_length=64, blank=True)
    birthdate = models.DateField(_('Birth date'), null=True, blank=True)
    country = models.ForeignKey(Country, null=True, blank=True,
                                verbose_name=_('Country'))
    uuid = models.CharField(max_length=64, blank=True)

    class Meta:
        abstract = True

    def __unicode__(self):  # pragma: no cover
        return self.name or ''

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(BasePerson, self).save(*args, **kwargs)

    @property
    def age(self):
        n = timezone.now()
        years = n.year - self.birthdate.year
        if self.birthdate.month < n.month:
            years -= 1

        elif self.birthdate.month == n.month and self.birthdate.day < n.day:
            years -= 1
        return years

class Person(BasePerson, QuerysetUnionMixin):
    """ Meta model to just query over all BasePerson subclasses.
    """

    class Meta:
        abstract = True


class FilmDirector(BasePerson):
    """ The guy who directs the whole thing.
    """

    class Meta:
        verbose_name = _('Film director')
        verbose_name_plural = _('Film directors')


class Actor(BasePerson):
    """ Someone who plays a role in a film.
    """
    GENDERS = (
        (0, _('Female')),
        (1, _('Male')),
        (2, _('Other')),
    )

    gender = models.IntegerField(_('Gender'), choices=GENDERS, default=0)

    class Meta:
        verbose_name = _('Actor/Actress')
        verbose_name_plural = _('Actors & Actresses')

    def get_absolute_url(self):
        url = urlresolvers.reverse('mdcms.people.views.show_actor',
                                   args=[self.slug])
        return url
