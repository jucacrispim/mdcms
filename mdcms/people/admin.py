# -*- coding: utf-8 -*-

from django.db import models
from django.contrib import admin
from django.utils.translation import ugettext as _
from mdcms import widgets
from mdcms.people.models import FilmDirector, Actor


class FilmDirectorAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ImageField: {'widget': widgets.ImgClearableFileInput}
    }

    fieldsets =  (
        (None, {
            'fields': ('name', 'image', 'birthdate', 'country')
        }),
    )



class ActorAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ImageField: {'widget': widgets.ImgClearableFileInput}
    }

    fieldsets =  (
        (None, {
            'fields': ('name', 'image', 'gender', 'birthdate', 'country')
        }),
    )


admin.site.register(FilmDirector, FilmDirectorAdmin)
admin.site.register(Actor, ActorAdmin)
