# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name='Name')),
                ('country', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_pt_br', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_en', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_it', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_fr', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_es', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('birthdate', models.DateTimeField(null=True, verbose_name='Birth date', blank=True)),
                ('gender', models.IntegerField(default=0, verbose_name='Gender', choices=[(0, 'Female'), (1, 'Male'), (2, 'Other')])),
            ],
            options={
                'verbose_name': 'Actor',
                'verbose_name_plural': 'Actors',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FilmDirector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name='Name')),
                ('country', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_pt_br', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_en', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_it', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_fr', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
                ('country_es', models.CharField(max_length=32, null=True, verbose_name='Country of origin', blank=True)),
            ],
            options={
                'verbose_name': 'Film director',
                'verbose_name_plural': 'Film directors',
            },
            bases=(models.Model,),
        ),
    ]
