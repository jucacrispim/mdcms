# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0004_auto_20150220_0352'),
    ]

    operations = [
        migrations.AddField(
            model_name='actor',
            name='image',
            field=models.ImageField(upload_to=b'static/uploads/%Y/%m/%d/', null=True, verbose_name='Image', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filmdirector',
            name='image',
            field=models.ImageField(upload_to=b'static/uploads/%Y/%m/%d/', null=True, verbose_name='Image', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actor',
            name='birthdate',
            field=models.DateField(null=True, verbose_name='Birth date', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actor',
            name='country',
            field=models.ForeignKey(verbose_name='Country', blank=True, to='mdcms.Country', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filmdirector',
            name='birthdate',
            field=models.DateField(null=True, verbose_name='Birth date', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filmdirector',
            name='country',
            field=models.ForeignKey(verbose_name='Country', blank=True, to='mdcms.Country', null=True),
            preserve_default=True,
        ),
    ]
