# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0002_auto_20150219_1358'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='actor',
            options={'verbose_name': 'Actor/Actress', 'verbose_name_plural': 'Actors & Actresses'},
        ),
        migrations.AddField(
            model_name='actor',
            name='uuid',
            field=models.CharField(default='1as33f341', max_length=64),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='filmdirector',
            name='birthdate',
            field=models.DateTimeField(null=True, verbose_name='Birth date', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filmdirector',
            name='uuid',
            field=models.CharField(default=1, max_length=64),
            preserve_default=False,
        ),
    ]
