# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0005_auto_20150220_1240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actor',
            name='slug',
            field=models.CharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actor',
            name='uuid',
            field=models.CharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filmdirector',
            name='slug',
            field=models.CharField(max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filmdirector',
            name='uuid',
            field=models.CharField(max_length=64, blank=True),
            preserve_default=True,
        ),
    ]
