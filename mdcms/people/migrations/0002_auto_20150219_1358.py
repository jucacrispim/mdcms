# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('mdcms', '0005_country'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='actor',
            name='country_en',
        ),
        migrations.RemoveField(
            model_name='actor',
            name='country_es',
        ),
        migrations.RemoveField(
            model_name='actor',
            name='country_fr',
        ),
        migrations.RemoveField(
            model_name='actor',
            name='country_it',
        ),
        migrations.RemoveField(
            model_name='actor',
            name='country_pt_br',
        ),
        migrations.RemoveField(
            model_name='filmdirector',
            name='country_en',
        ),
        migrations.RemoveField(
            model_name='filmdirector',
            name='country_es',
        ),
        migrations.RemoveField(
            model_name='filmdirector',
            name='country_fr',
        ),
        migrations.RemoveField(
            model_name='filmdirector',
            name='country_it',
        ),
        migrations.RemoveField(
            model_name='filmdirector',
            name='country_pt_br',
        ),
        migrations.AlterField(
            model_name='actor',
            name='country',
            field=models.ForeignKey(blank=True, to='mdcms.Country', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='filmdirector',
            name='country',
            field=models.ForeignKey(blank=True, to='mdcms.Country', null=True),
            preserve_default=True,
        ),
    ]
