# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    # show actor
    url(r'^actor/(?P<slug>.+)', 'mdcms.people.views.show_actor',
        name='show_actor'),

)
