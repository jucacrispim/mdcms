# -*- coding: utf-8 -*-

import json
from uuid import uuid4
from django.utils import timezone
from django.core.validators import validate_email
from django.core import urlresolvers
from django.conf import settings
from django.contrib.auth import authenticate
from django.http import HttpResponse, HttpResponseServerError
from django.utils.translation import ugettext as _
from django.template.loader import render_to_string
from django_merceariadigital.controllers import OrderController
from django_merceariadigital.utils import send_mail
from mdcms.exceptions import ImproperlyConfiguredController
from mdcms.models import User, Login, PurchasePlan
from mdcms.base.controllers import BaseModelController


class PurchasePlanController(BaseModelController):
    model = PurchasePlan

    def buy(self, user):
        now = timezone.now()
        plan = self.request.REQUEST.get('plan')
        plan = PurchasePlan.objects.get(id=plan)
        order_data = {'init_date': now,
                      'end_date': plan.get_end_date(now),
                      'token': str(uuid4()),
                      'value': plan.value}

        user_info = {'name': user.username, 'email': user.email,
                     'identity': user.token}

        order_data.update({'user_token': user.token})
        order_controller = OrderController(self.request)
        order = order_controller.create(**order_data)
        login_controller = LoginController(self.request)
        context = {'login_token': ''}
        return order_controller.show_payment_post_form(order, user_info,
                                                       **context)

# pretty shit place for this controller.
class BaseController(object):

    def __init__(self, request):
        self.request = request

    def _return_error_json(self, msg):
        """ Returns a json with a http status 500
        :param msg: mesage to user
        """
        err = {'status': 500,
               'msg': msg}
        return HttpResponseServerError(json.dumps(err),
                                       content_type='application/json')

    def _return_json(self, msg):
        """ Returns a json with a http status 200
        :param msg: mesage to user
        """

        ret = {'status': 200,
               'msg': msg}
        return HttpResponse(json.dumps(ret),
                            content_type='application/json')


class UserController(BaseController):
    """ Controller that handle users.
    """

    def create(self, sendmail=False):
        """Creates a new user
        """
        email = self.request.POST.get('email')
        validate_email(email)
        passwd = self.request.POST.get('password')
        username = self.request.POST.get('username') or email

        if not email or not passwd:
            raise Exception(_('Email AND password are required'))

        if User.objects.filter(email=email):
            raise Exception(_('User already registered'))

        user = User(email=email, username=username)
        user.set_password(passwd)
        user.save()
        if sendmail:
            subject = _('Welcome!')
            html_message = render_to_string('mdcms/email/welcome.html')
            msg = _('Welcome')
            email = user.email
            mail_to = [email]
            send_mail(subject, msg, mail_to, html_message=html_message)
        return user

    def get_user(self, **kwargs):
        """ Returns a User instance
        """
        user = User.objects.get(**kwargs)
        return user

    def check_perms(self, user):
        """ Check wich permissions a user have
        """
        msg = 'can watch videos' if user.can_watch_videos \
              else 'cannot watch videos'

        return self._return_json(msg)

    def change_password(self, sendmail=False):
        """ Changes an users' password
        """
        change_password_token = self.request.POST.get('change_password_token')

        try:
            user = self.get_user(change_password_token=change_password_token)
        except Exception as e:
            return self._return_error_json(_('Invalid token'))

        new_password = self.request.POST.get('new_password')
        try:
            user.change_password(change_password_token, new_password)
        except Exception as e:
            return self._return_error_json(e.message)

        if sendmail:
            subject = _('Password change')
            message = subject
            mail_to = [user.email]
            html_message = render_to_string('mdcms/email/changepasswordok.html')
            send_mail(subject, message, mail_to, html_message=html_message)

        return self._return_json(_('Password changed successfully'))

    def request_change_password_url(self, sendmail=True):
        """ Creates a url for changing password.
        :param email: If the link should be sent by email
        """
        eternal_msg = 'If this email is in our database, an email '
        eternal_msg += 'was sent with a link to change your password'
        eternal_msg = _(eternal_msg)

        email = self.request.POST.get('email')
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return self._return_json(eternal_msg)

        token = user.generate_change_password_token()
        url = urlresolvers.reverse(
            'chpasswd', kwargs={'change_password_token': token})
        url = settings.BASE_URL + url

        if sendmail:
            subject = _('Change password request')
            msg = 'Chage password link'
            msg += url
            msg = _(msg)
            context = {'link': url}
            html_message = render_to_string('mdcms/email/change_password.html',
                                            context)
            mail_to = [email]
            send_mail(subject, msg, mail_to, html_message=html_message)

        return url


    def request_change_password_token(self, return_json=True, user=None):
        """ Creates a new change password token for an user.
        """
        if not user:
            user = self._get_logged_user()

        token = user.generate_change_password_token()
        if return_json:
            return self._return_json(token)
        else:
            return token

    def is_logged(self):
        logged = False
        try:
            self._get_logged_user()
            logged = True
        except:
            pass
        return logged

    def change_email(self, sendmail=False):
        """ Changes an users' email
        """
        user = self._get_logged_user()
        new_email = self.request.POST.get('email')
        user.email = new_email
        user.save()
        if sendmail:
            subject = _('Email changed')
            msg = subject
            html_message = render_to_string('mdcms/email/changeemailok.html')
            mail_to = [user.email]
            send_mail(subject, msg, mail_to, html_message=html_message)

        return self._return_json(_('Email changed successfully'))

    def _get_login(self):
        """ Returns the current Login instance for the request
        """

        login_token = self.request.COOKIES.get('cmsexlogin', '').strip()
        if not login_token:
            msg = 'no login token'
            raise Exception(msg)

        try:
            login = Login.objects.get(token=login_token)
        except:
            msg = 'Login not found'
            raise Exception(msg)
        return login

    def _get_logged_user(self):
        """ Returns the logged user on site
        """
        return self._get_login().user


class LoginController(BaseController):
    """ Controller that handles login stuff.
    """

    def login(self):
        """ Do login... I guess.
        """
        email =  self.request.POST.get('email').strip()
        passwd = self.request.POST.get('password').strip()


        try:
            self._validate_login_data(email, passwd)
        except Exception as e:
            msg = e.message
            self._return_error_json(msg)

        user = authenticate(username=email, password=passwd)
        if user is None:
            msg = _('Invalid credentials')
            return self._return_error_json(msg)

        # authenticate returns a django user, we need a merceariadigital user
        user = User.objects.get(id=user.id)
        # everything fine and dandy!
        return self._create_login(user)

    def logout(self):
        """ Guess what?
        """
        try:
            login = self._get_login()
        except Exception as e:
            return self._return_error_json(e.message)

        login.delete()
        return self._return_json('logout ok')

    def _create_login(self, user):
        """ Creates a Login instance.
        :param user: user that will be logged in
        """
        login = Login()
        login.create = timezone.now()
        login.expire =  login.create + timezone.timedelta(days=99)
        login.ip = self.request.META.get('REMOTE_ADDR')
        login.user = user
        login.save()

        return HttpResponse(login.token)

    def _validate_login_data(self, email, passwd):
        """ Validades if there are email and password and if email is a
        valid one.
        """

        if not email or not passwd:
            raise Exception(_('Email AND password are required'))

        try:
            validate_email(email)
            valid_email = True
        except:
            valid_email = False

        if not valid_email:
            raise Exception(_('Invalid email'))
        return True

    def is_valid(self):
        token = self.request.REQUEST.get('token').strip()
        if not token:
            return False

        return Login.is_valid(token)
