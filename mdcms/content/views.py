# -*- coding: utf-8 -*-

import importlib
from django.conf import settings
from django.shortcuts import render
from django.template import RequestContext
from django.utils.translation import ugettext as _
from mdcms.exceptions import UnknownCtypeForView
from mdcms.controllers import UserController
from mdcms.content.controllers import (FilmController, CategoryController,
                                       TagController)


def get_html_view(ctype):
    """ Returns a class view for ctype
    :param ctype: Type of content (by now video only)
    """

    if ctype == 'video':
        default_view = 'mdcms.content.views.FilmHTMLView'
        setttings_var = 'VIDEO_HTML_VIEW'
    else:
        raise UnknownCtypeForView(_('Unknown ctype: %s' % ctype))

    try:
        html_view = getattr(settings, setttings_var)
    except AttributeError:
        html_view = default_view

    mod, cls = html_view.rsplit('.', 1)
    mod = importlib.import_module(mod)
    cls = getattr(mod, cls)
    return cls


class FilmHTMLView(object):
    def __init__(self, request):
        self.controller = FilmController(request)
        self.show_template = 'mdcms/content/show_film.html'
        self.list_template = 'mdcms/content/list_films.html'
        self.video_player_template = 'mdcms/content/video_player.html'
        self.video_player_denied_template = 'mdcms/content/video_player_denied.html'

    def show(self, slug):
        context = {}
        context['content'] = self.controller.get(slug)
        extra = self.get_show_extra_context(context['content'])
        context.update(extra)
        return self.render_template(self.show_template, **context)

    def show_player(self, slug):
        user = UserController(self.controller.request)._get_logged_user()
        if not user.can_watch_videos:
            return self.render_template(self.video_player_denied_template)

        context = {}
        context['content'] = self.controller.get(slug)
        extra = self.get_show_player_extra_context(context['content'])
        context.update(extra)
        return self.render_template(self.video_player_template, **context)

    def get_show_extra_context(self, content):
        """ Extra context to show content. Overide it in
        your own class.
        """
        return {}

    def get_show_player_extra_context(self, content):
        """ Extra context to show player for content. Overide it in
        your own class.
        """
        return {}

    def list(self, first=0, last=0, exclude_uuids=None, **kwargs):
        """ List films using ``first`` and ``last`` as slice index
        """
        clist = self.controller.get_list(first, last, exclude_uuids, **kwargs)
        context = {}
        context['contents'] = clist
        extra = self.get_list_extra_context(context)
        context.update(extra)
        return self.render_template(self.list_template, **context)

    def get_list_extra_context(self, context):
        """ Extra context to list content. Overide it in
        your own class.
        """
        return {}

    def render_template(self, template_name, **context):
        """ Renders a template using context
        :param template_name: name for the template
        :param context: dictionary containing the context for the template
        """
        return render(
            self.controller.request, template_name, context,
            context_instance=RequestContext(self.controller.request))


def show_content(request, ctype, slug):
    view_class = get_html_view(ctype)
    view = view_class(request)
    return view.show(slug)


def list_films(request, first=0, last=8):
    exclude_uuids = request.GET.getlist('euuid')
    view_class = get_html_view('video')
    view = view_class(request)
    return view.list(first, last, exclude_uuids=exclude_uuids)


def list_films_by_category(request, cat_slug, first=0, last=8):
    exclude_uuids = request.GET.getlist('euuid')
    view_class = get_html_view('video')
    view = view_class(request)
    controller = CategoryController(request)
    category = controller.get(slug=cat_slug)
    return view.list(categories=category, first=first, last=last,
                     exclude_uuids=exclude_uuids)


def list_films_by_tag(request, tag_slug, first=0, last=8):
    exclude_uuids = request.GET.getlist('euuid')
    view_class = get_html_view('video')
    view = view_class(request)
    controller = TagController(request)
    category = controller.get(slug=tag_slug)
    return view.list(categories=category, first=first, last=last,
                     exclude_uuids=exclude_uuids)


def show_player(request, slug):
    """ Show a video player.
    """
    view_class = get_html_view('video')
    view = view_class(request)
    return view.show_player(slug)
