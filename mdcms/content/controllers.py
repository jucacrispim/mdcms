# -*- coding: utf-8 -*-

from mdcms.controllers import BaseModelController
from mdcms.content.models import Content, Film, Category, Tag


class TagController(BaseModelController):
    model = Tag


class CategoryController(BaseModelController):
    model = Category


class ContentController(BaseModelController):
    model = Content


class FilmController(BaseModelController):
    model = Film
