# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib import admin

urlpatterns = patterns(
    '',

    # list videos by tag
    url(r'^videos/tag/(?P<tag_slug>\w+?)/$',
        'mdcms.content.views.list_films_by_tag',
        name='list_films_by_tag'),

    url(r'^videos/tag/(?P<tag_slug>\w+?)/(?P<first>\d+)/(?P<last>\d+)/$',
        'mdcms.content.views.list_films_by_tag',
        name='list_films_by_tag_page'),

    # list videos by category
    url(r'^videos/(?P<cat_slug>\w+?)/$',
        'mdcms.content.views.list_films_by_category',
        name='list_films_by_category'),

    url(r'^videos/(?P<cat_slug>\w+?)/(?P<first>\d+)/(?P<last>\d+)/$',
        'mdcms.content.views.list_films_by_category',
        name='list_films_by_category_page'),

    # show player video
    url(r'^video/(?P<slug>.+)/play$',
        'mdcms.content.views.show_player', name='show_player'),

    # show content
    url(r'^(?P<ctype>\w+)/(?P<slug>.+)', 'mdcms.content.views.show_content',
        name='show_content'),

)
