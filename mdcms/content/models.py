# -*- coding: utf-8 -*-

import uuid
from django.db import models
from django.conf import settings
from django.core import urlresolvers
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.utils.text import slugify
from mdcms.mixins import QuerysetUnionMixin
from mdcms.people.models import FilmDirector, Actor


class Category(models.Model):
    """ Category for content
    """

    name = models.CharField(_('Name'), max_length=32)
    parent = models.ForeignKey('Category', null=True, blank=True)
    slug = models.CharField(max_length=128, unique=True)

    def __unicode__(self):  # pragma: no cover
        return self.name or ''

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


class Tag(models.Model):
    """ Keyword for content
    """
    tag = models.CharField(_('Tag'), max_length=32, unique=True)
    slug = models.CharField(max_length=128, unique=True)

    def __unicode__(self):  # pragma: no cover
        return self.tag or ''

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.tag)
        super(Tag, self).save(*args, **kwargs)




class BaseContent(models.Model):
    """ Base abstract model for any kind of content.
    """


    STATUS = (
        (0, _('Hidden')),
        (1, _('Published')),
        (2, _('Draft')),
    )

    title = models.CharField(_('Title'), max_length=64)
    description = models.CharField(_('Description'), max_length=256,
                                   null=True, blank=True)
    slug = models.CharField(max_length=128, unique=True)
    uuid = models.CharField(max_length=64, unique=True)
    status = models.IntegerField(choices=STATUS, default=2)
    creation = models.DateTimeField(_('Creation date'))
    publication = models.DateTimeField(_('Publication date'),
                                       null=True, blank=True)
    update = models.DateTimeField(_('Date of last update'),
                                  null=True, blank=True)
    categories = models.ManyToManyField(Category, verbose_name=_('Categories'))
    tags = models.ManyToManyField(Tag)

    class Meta:
        abstract = True


    def __unicode__(self):  # pragma: no cover
        return self.title or ''

    def save(self, *args, **kwargs):
        now = timezone.now()
        if not self.creation:
            self.creation = now

        if self.status == 1 and not self.publication:
            self.publication = now

        self.update = now

        if not self.slug:
            self.slug = slugify(self.title)

        if not self.uuid:
            self.uuid = str(uuid.uuid4())

        super(BaseContent, self).save(*args, **kwargs)


class Content(BaseContent, QuerysetUnionMixin):
    """ Meta model to just query over all BaseContent subclasses.
    """

    class Meta:
        abstract = True


class Film(BaseContent):
    """ A motion picture; a movie.
    """

    video_hd = models.URLField('Video HD - 1080p',
                                null=True, blank=True)
    video_sd = models.URLField('Video SD - 480p',
                                null=True, blank=True)
    thumbnail = models.ImageField(_('Thumbnail') + ' - 255x340',
                                  upload_to=settings.UPLOAD_PATH)
    video_cover = models.ImageField(_('Video Cover') + ' - 818x462',
                                    upload_to=settings.UPLOAD_PATH)

    directors = models.ManyToManyField(FilmDirector, null=True, blank=True,
                                       verbose_name=_('Directors'))
    cast = models.ManyToManyField(Actor, null=True, blank=True,
                                  verbose_name=_('Cast'))

    ctype = 'video'

    class Meta:
        verbose_name = _('Film')
        verbose_name_plural = _('Films')

    def get_absolute_url(self):
        url = urlresolvers.reverse('mdcms.content.views.show_content',
                                   args=[self.ctype, self.slug])
        return url

    def admin_thumbnail(self):
        return u'<img src="%s" width="100" />' % (self.thumbnail.url)

    admin_thumbnail.short_description = _('Thumbnail')
    admin_thumbnail.allow_tags = True
