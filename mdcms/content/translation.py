# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions
from mdcms.content import models


class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)


class TagTranslationOptions(TranslationOptions):
    fields = ('tag',)


class FilmTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


translator.register(models.Category, CategoryTranslationOptions)
translator.register(models.Tag, TagTranslationOptions)
translator.register(models.Film, FilmTranslationOptions)
