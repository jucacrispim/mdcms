# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0002_auto_20150219_0001'),
    ]

    operations = [
        migrations.AddField(
            model_name='film',
            name='uuid',
            field=models.CharField(default='sdf', unique=True, max_length=64),
            preserve_default=False,
        ),
    ]
