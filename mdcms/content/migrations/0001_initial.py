# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, verbose_name='Name')),
                ('name_pt_br', models.CharField(max_length=32, null=True, verbose_name='Name')),
                ('name_en', models.CharField(max_length=32, null=True, verbose_name='Name')),
                ('name_it', models.CharField(max_length=32, null=True, verbose_name='Name')),
                ('name_fr', models.CharField(max_length=32, null=True, verbose_name='Name')),
                ('name_es', models.CharField(max_length=32, null=True, verbose_name='Name')),
                ('parent', models.ForeignKey(blank=True, to='content.Category', null=True)),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Film',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=64, verbose_name='Title')),
                ('title_pt_br', models.CharField(max_length=64, null=True, verbose_name='Title')),
                ('title_en', models.CharField(max_length=64, null=True, verbose_name='Title')),
                ('title_it', models.CharField(max_length=64, null=True, verbose_name='Title')),
                ('title_fr', models.CharField(max_length=64, null=True, verbose_name='Title')),
                ('title_es', models.CharField(max_length=64, null=True, verbose_name='Title')),
                ('description', models.CharField(max_length=256, null=True, verbose_name='Description', blank=True)),
                ('description_pt_br', models.CharField(max_length=256, null=True, verbose_name='Description', blank=True)),
                ('description_en', models.CharField(max_length=256, null=True, verbose_name='Description', blank=True)),
                ('description_it', models.CharField(max_length=256, null=True, verbose_name='Description', blank=True)),
                ('description_fr', models.CharField(max_length=256, null=True, verbose_name='Description', blank=True)),
                ('description_es', models.CharField(max_length=256, null=True, verbose_name='Description', blank=True)),
                ('slug', models.CharField(unique=True, max_length=64)),
                ('status', models.IntegerField(default=2, choices=[(0, 'Hidden'), (1, 'Published'), (2, 'Draft')])),
                ('creation', models.DateTimeField(verbose_name='Creation date')),
                ('publication', models.DateTimeField(null=True, verbose_name='Publication date', blank=True)),
                ('update', models.DateTimeField(null=True, verbose_name='Date of last update', blank=True)),
                ('video_hd', models.URLField(null=True, verbose_name=b'Video HD - 1080p', blank=True)),
                ('video_sd', models.URLField(null=True, verbose_name=b'Video SD - 480p', blank=True)),
                ('thumbnail', models.ImageField(upload_to=b'static/uploads/%Y/%m/%d/', verbose_name='Thumbnail - 255x340')),
                ('video_cover', models.ImageField(upload_to=b'static/uploads/%Y/%m/%d/', verbose_name='Video Cover - 818x462')),
                ('cast', models.ManyToManyField(to='people.Actor', null=True, blank=True)),
                ('categories', models.ManyToManyField(to='content.Category')),
                ('directors', models.ManyToManyField(to='people.FilmDirector', null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(unique=True, max_length=32, verbose_name='Tag')),
                ('tag_pt_br', models.CharField(max_length=32, unique=True, null=True, verbose_name='Tag')),
                ('tag_en', models.CharField(max_length=32, unique=True, null=True, verbose_name='Tag')),
                ('tag_it', models.CharField(max_length=32, unique=True, null=True, verbose_name='Tag')),
                ('tag_fr', models.CharField(max_length=32, unique=True, null=True, verbose_name='Tag')),
                ('tag_es', models.CharField(max_length=32, unique=True, null=True, verbose_name='Tag')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='film',
            name='tags',
            field=models.ManyToManyField(to='content.Tag'),
            preserve_default=True,
        ),
    ]
