# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='slug',
            field=models.CharField(default=1, unique=True, max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tag',
            name='slug',
            field=models.CharField(default='sl-s', unique=True, max_length=128),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='film',
            name='slug',
            field=models.CharField(unique=True, max_length=128),
            preserve_default=True,
        ),
    ]
