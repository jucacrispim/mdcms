# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0004_auto_20150219_1343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='film',
            name='cast',
            field=models.ManyToManyField(to='people.Actor', null=True, verbose_name='Cast', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='categories',
            field=models.ManyToManyField(to='content.Category', verbose_name='Categories'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='film',
            name='directors',
            field=models.ManyToManyField(to='people.FilmDirector', null=True, verbose_name='Directors', blank=True),
            preserve_default=True,
        ),
    ]
