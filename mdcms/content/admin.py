# -*- coding: utf-8 -*-
from django.db import models
from django.contrib import admin
from django.utils.translation import ugettext as _
from mdcms import widgets
from mdcms.content.models import Film, Category, Tag


class CategoryAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name_pt_br', )}),

        (_('Translations'), {
            'classes': ('collapse', ),
            'fields': ('name_en', 'name_es', 'name_fr',
                       'name_it')}),
    )


class TagAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('tag_pt_br',)}),

        (_('Translations'), {
            'classes': ('collapse', ),
            'fields': ('tag_en', 'tag_es', 'tag_fr',
                       'tag_it')}),
    )


class FilmAdmin(admin.ModelAdmin):
    list_display = ('title', 'admin_thumbnail',  'status', 'publication',)
    list_display_links = ('title', 'admin_thumbnail', 'status', 'publication',)
    search_fields = ('title',)
    ordering = ('-id',)
#    exclude = ('slug', 'created', 'published',)
    list_filter = ('status', 'categories')
    filter_horizontal = ('tags', 'categories')

    formfield_overrides = {
            models.ImageField: {'widget': widgets.ImgClearableFileInput}
    }

    fieldsets = (
        (None, {
            'fields': ('title_pt_br', 'description_pt_br')}),

        (_('Translations'), {
            'classes': ('collapse', ),
            'fields': ('title_en', 'description_en',
                       'title_es', 'description_es',
                       'title_fr', 'description_fr',
                       'title_it', 'description_it')}),
        (_('General info'), {
            'fields': ('slug', 'video_hd', 'video_sd', 'video_cover',
                       'thumbnail', 'status', 'categories', 'tags',
                       'directors', 'cast')})

    )


admin.site.register(Category, CategoryAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Film, FilmAdmin)
