��    M      �  g   �      �     �     �  
   �     �     �  
   �     �     �  	   �  	                  %     +     ?     C  	   O     Y     _          �  
   �     �     �     �     �     �     �     �     �     �                    0     D     R     `     h     w     |     �     �     �     �     �     �     �  
   �     �     �     �     �  	   	     	     &	     9	     H	     P	  	   T	     ^	     d	     q	     w	     �	     �	     �	     �	     �	     �	     �	     �	      
     	
     
  /   
  t  E
  
   �     �     �     �     �  
     	          
   :     E     M     S     e     m     �     �     �     �  !   �     �     �                    %     +     <     O     V     _     d  
   w     �     �     �     �     �     �     �  	   �                    !     &     ,     2     @     [     h     q     �     �  	   �     �      �     �     �     �  	   �     �                      9     Z     p     �     �     �     �  	   �  
   �     �     �  ,   �     ?                    9                   8   K                            3   L      J          M       ,   A   
   @       *      0      E   :      H   C   I   >   1          /   #         "       %             G   F   +   (      2       &          =   ;                 	   D   <       4   .              B   -       5          6   '       )       !   $                     7    Actor/Actress Actors & Actresses Birth date Brazilian Portuguese Cast Categories Category Change password request Confirmed Countries Country Creation date Daily Date of last update Day Description Directors Draft Email AND password are required Email changed Email changed successfully Email sent English Female Film Film director Film directors Films French Gender General info Hidden Image Invalid change_password_token Invalid credentials Invalid email Invalid token Italian Main Highlight Male Month Monthly Most Searched Name Order Other Password change Password changed successfully Payment ok Period Period type Picture Publication date Published Purchase plan Purchase plan info Purchase plans Spanish Tag Thumbnail Title Translations Trial Trial period Trial period type Trial value Unknown ctype: %s User already registered Value in Dollars Value in Reais Video Cover Welcome Welcome! Year Yearly You do not have permission to watch this video. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-04-02 04:08+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Juca Crispim <juca@poraodojuca.net>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Ator/Atriz Atores & Atrizes Data de nascimento Português brasileiro Elenco Categorias Categoria Pedido de alteração de senha Confirmado Países País Data de criação Diário Data da última atualização Dia Descrição Diretores de filme Rascunho E-mail E senha são obrigatórios E-mail alterado E-mail alterado com sucesso E-mail enviado Inglês Feminino Filme Diretor de filme Diretores de filme Filmes Francês Sexo Informação geral Invisível Imagem change_password_token inválido Credenciais inválidas E-mail inválido Token inválido Italiano Destaque Principal Masculino Mês Mensal Mais buscados Nome Ordem Outro Alterar senha Senha alterada com sucesso Pagamento ok Período Tipo do período Foto Data de publicação Publicado Plano de compra Informações do plano de compra Planos de compra Espanhol Tag Miniatura Título Traduções Degustação Período de degustação Tipo do período de degustação Valor da degustação ctype desconhecido %s Usuário já registrado Valor em dólares Valor em reais Capa do vídeo Bem-vindo Bem-vindo! Ano Anual Você não tem permissão de ver este vídeo 