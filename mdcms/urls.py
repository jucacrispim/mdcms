from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'mdcms.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # send an email with an url to change password
    url(r'^user/request-change-password/$',
        'mdcms.views.request_change_password_url', name='chpasswdurl'),

    # form to change password without being authenticated.
    # this url (with token) is sent by email
    url(r'^user/chpasswd/(?P<change_password_token>.*)/',
        'mdcms.views.show_change_password_by_url_form', name='chpasswd'),

    # url used by authenticated users change their passwords
    url(r'^user/change_password/',
        'mdcms.views.change_password', name='change-password'),

    # url used by authenticated users change their emails
    url(r'^user/change_email/',
        'mdcms.views.change_email', name='change-email'),

    # returns a change password token for an user
    url(r'^user/request-chpasswd-token/',
        'mdcms.views.request_change_password_token',
        name='request-chpasswd-token'),

    # list purchase plans
    url(r'^purchaseplans/',
        'mdcms.views.list_plans',
        name='list-purchase-plans'),

    # purchase a plan
    url(r'^buy/',
        'mdcms.views.buy',
        name='buy-plan'),


)
