# -*- coding: utf-8 -*-

import uuid
import unittest
import mock
from mdcms import models, controllers


class UserControllerTest(unittest.TestCase):
    def setUp(self):
        request = mock.Mock()
        request.POST = {'email': 'a@a.com', 'password': 'asdf'}
        self.controller = controllers.UserController(request)

    def tearDown(self):
        models.User.objects.all().delete()

    def test_create_user_without_required_params(self):
        """ Ensure that an user can't be create without required params.
        """
        self.controller.request.POST = {}
        with self.assertRaises(Exception):
            self.controller.create()

    def test_create_user(self):
        """ Ensure that a user can be created properly
        """
        user = self.controller.create()
        self.assertTrue(user.id)

    def test_create_user_with_email_already_used(self):
        """ Ensure that a user can't be create with an email already used.
        """
        user = self.controller.create()
        with self.assertRaises(Exception):
            user = self.controller.create()

    def test_get_user(self):
        """ Ensure that get_user method works properly
        """
        u = self.controller.create()
        nu = self.controller.get_user(token=u.token)

        self.assertEqual(u.id, nu.id)

    def test_change_password_with_wrong_token(self):
        """ Ensure that password can't be changed with a wrong token
        """
        u = self.controller.create()
        self.controller._get_logged_user = mock.Mock()
        self.controller._get_logged_user.return_value = u
        with self.assertRaises(Exception):
            u.change_password('invalid-token', 'newpass')

    def test_change_password(self):
        """ Ensure that the password can be changed
        """
        u = self.controller.create()
        self.controller._get_logged_user = mock.Mock()
        self.controller._get_logged_user.return_value = u
        change_password_token = self.controller.request_change_password_token(
            return_json=False)
        self.controller.request.POST['new_password'] = 'newpass'
        self.controller.request.POST[
            'change_password_token'] = change_password_token

        self.controller.change_password()

        u = self.controller.get_user(id=u.id)
        self.assertTrue(u.check_password('newpass'))

    def test_request_change_password_url(self):
        """ Ensure that request_change_password_url method works properly
        """
        u = self.controller.create()
        url = self.controller.request_change_password_url(sendmail=False)
        self.assertIn('/user/chpasswd/', url)

    @mock.patch.object(controllers, 'send_mail',
                       mock.Mock(spec=controllers.send_mail))
    def test_request_change_password_url_send_email(self):
        """ Ensure that request_change_password_url method can send email
        """
        u = self.controller.create()
        url = self.controller.request_change_password_url()
        self.assertIn('/user/chpasswd/', url)
        self.assertTrue(controllers.send_mail.called)

    def test_request_change_password_token(self):
        """ Ensure that the request_change_password_token works properly
        """
        u = self.controller.create()
        self.controller._get_logged_user = mock.Mock()
        self.controller._get_logged_user.return_value = u
        token = self.controller.request_change_password_token(
            return_json=False)
        u = models.User.objects.get(id=u.id)
        self.assertEqual(u.change_password_token, token)


class LoginControllerTest(unittest.TestCase):
    def setUp(self):
        request = mock.Mock()
        self.controller = controllers.LoginController(request)

    def tearDown(self):
        models.User.objects.all().delete()
        models.Login.objects.all().delete()

    def test_validate_login_data_with_missing_data(self):
        """ Ensure that _validate_login_data raises properly when missing data
        """
        email = ''
        passwd = ''

        with self.assertRaises(Exception):
            self.controller._validate_login_data(email, passwd)


    def test_validate_login_data_with_invalid_email(self):
        """ Ensure that _validate_login_data raises properly with invalid email
        """
        email = 'a'
        passwd = 'asdf'

        with self.assertRaises(Exception):
            self.controller._validate_login_data(email, passwd)

    def test_validate_login_data(self):
        """ Ensure that _validate_login_data works properly with ok data.
        """

        email = 'a@a.com'
        passwd = 'adf'

        self.assertTrue(self.controller._validate_login_data(email, passwd))

    @mock.patch.object(controllers, 'authenticate', mock.Mock())
    def test_login(self):
        """ Ensure that a login is created
        """
        user = models.User(email='a@a.com')
        user.save()
        controllers.authenticate.return_value = user
        email = 'a@a.com'
        passwd = 'asdf'
        self.controller.request.POST = {'email': email, 'password': passwd}

        ret = self.controller.login()
        login_token = ret.content

        login = models.Login.objects.get(token=login_token)
        self.assertTrue(login.id)
