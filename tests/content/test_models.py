# -*- coding: utf-8 -*-

import unittest
from mdcms.content import models


class ContentTest(unittest.TestCase):
    def tearDown(self):
        models.Content.objects.all().delete()

    def test_query_content(self):
        """ Ensure that objects from subclasses are returned
        """

        film = models.Film(title='title', slug='title')
        film.save()

        self.assertEqual(len(models.Content.objects), 1)


class FilmTest(unittest.TestCase):
    def tearDown(self):
        models.Film.objects.all().delete()

    def test_save_without_creation_date(self):
        """ Ensure that creation date is setted correctly on save() method
        """

        content = models.Film(title='title', slug='slug')
        content.save()
        self.assertTrue(content.creation)


    def test_publish_wihtout_publication_date(self):
        """ Ensure that publication date is setted correctly on save()
        """

        content = models.Film(title='title', slug='slug', status=1)
        content.save()
        self.assertTrue(content.publication)
