# -*- coding: utf-8 -*-

import unittest
import mock
from mdcms import models
from django_merceariadigital import controllers
from django.utils import timezone


class PurchasePlanTest(unittest.TestCase):
    def tearDown(self):
        models.PurchasePlan.objects.all().delete()

    def test_create_plan_without_required_fields(self):
        """ Ensure that a PurchasePlan can't be created without required fields
        """

        plan = models.PurchasePlan(name='asdf')
        with self.assertRaises(Exception):
            plan.save()

    def test_create_plan(self):
        """ Ensure that a PurchasePlan can be created properly
        """

        plan = models.PurchasePlan(name='asdf', period=2,
                                   value=10.2)
        plan.save()
        self.assertTrue(plan.id)

    def test_get_end_date_with_months(self):
        """ Ensure that end_date() method works properly with months
        """

        plan = models.PurchasePlan()
        plan.name = 'adsf'
        plan.period = 1
        init = timezone.now()
        end = plan.get_end_date(init)

        self.assertEqual(end.month, init.month + 1)

    def test_get_end_date_with_days(self):
        """ Ensure that end_date() method works properly with days
        """

        plan = models.PurchasePlan()
        plan.name = 'adsf'
        plan.period = 1
        plan.period_type = 'D'
        init = timezone.now()
        end = plan.get_end_date(init)

        self.assertEqual(end.day, init.day + 1)
        self.assertEqual(end.month, init.month)
        self.assertEqual(end.year, init.year)

    def test_get_end_date_with_years(self):
        """ Ensure that end_date() method works properly with years
        """

        plan = models.PurchasePlan()
        plan.name = 'adsf'
        plan.period = 1
        plan.period_type = 'Y'
        init = timezone.now()
        end = plan.get_end_date(init)

        self.assertEqual(end.day, init.day)
        self.assertEqual(end.month, init.month)
        self.assertEqual(end.year, init.year + 1)


class UserTest(unittest.TestCase):
    def tearDown(self):
        models.User.objects.all().delete()

    def test_create_user_without_required_fields(self):
        """ Ensure that an user can't be created without required fields
        """

        user = models.User()
        with self.assertRaises(Exception):
            user.save()

    def test_create_user(self):
        """ Ensure that an user can be created properly
        """

        user = models.User(confirmed=True, email='a@a.com')

        user.save()

        self.assertTrue(user.token)

    def test_create_user_with_email_already_used(self):
        """ Ensure that a user can't be create with an email already used.
        """

        user = models.User(confirmed=True, email='a@a.com')

        user.save()
        user = models.User(confirmed=True, email='a@a.com')

        with self.assertRaises(Exception):
            user.save()

    def test_create_user_with_username_already_used(self):
        """ Ensure that a user can't be create with an username already used.
        """

        user = models.User(confirmed=True, email='a@a.com', username='ze')

        user.save()
        user = models.User(confirmed=True, email='a@b.com', username='ze')

        with self.assertRaises(Exception):
            user.save()

    def test_can_watch_videos(self):
        """ Ensure that can_watch_videos property works properly
        """
        user = models.User(email='asd@asd')
        self.assertFalse(user.can_watch_videos)
        user.payment_ok = True
        self.assertTrue(user.can_watch_videos)

    def test_user_orders(self):
        """ Ensure that orders property words properly
        """
        user = models.User(email='asdf@asdf.com')
        user.save()
        order_controller = controllers.OrderController(mock.Mock())
        order_data = {'init_date': timezone.now(),
                      'end_date': timezone.now() + timezone.timedelta(days=1),
                      'value': 10.0, 'user_token': user.token}
        order = order_controller.create(**order_data)

        self.assertTrue(len(user.orders), 1)

class LoginTestCase(unittest.TestCase):
    def tearDown(self):
        models.Login.objects.all().delete()
        models.User.objects.all().delete()

    def test_create_login_without_required_fields(self):
        """ Ensure that a login can not be create without required fields
        """
        l = models.Login()
        with self.assertRaises(Exception):
            l.save()

    def test_create_login(self):
        """ Ensure that a login can be created properly
        """
        user = models.User(email='a@a.com')
        user.save()
        now = timezone.now()
        l = models.Login(user=user, ip='127.0.0.1', create=now, expire=now)
        l.save()
        self.assertTrue(l.token)

    def test_is_valid_with_no_login(self):
        """ Ensure that a token not assigned to a Login is invalid
        """
        cls = mock.Mock()
        cls.objects.get.side_effects=[Exception]
        self.assertFalse(models.Login.is_valid('asdf'))

    def test_is_valid_with_expired_login(self):
        """ Ensure that an expired login is invalid
        """

        init = timezone.now() - timezone.timedelta(days=1000)
        expire = init + timezone.timedelta(days=99)
        user = models.User(email='adsf')
        user.save()
        login = models.Login(create=init, expire=expire, ip='127.0.0.1',
                             user=user)
        login.save()

        self.assertFalse(models.Login.is_valid(login.token))

    def test_is_valid_with_valid_login(self):
        """ Ensure that a valid login is valid. Duh.
        """

        init = timezone.now()
        expire = init + timezone.timedelta(days=99)
        user = models.User(email='adsf')
        user.save()
        login = models.Login(create=init, expire=expire, ip='127.0.0.1',
                             user=user)
        login.save()

        self.assertTrue(models.Login.is_valid(login.token))
