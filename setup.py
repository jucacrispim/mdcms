# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

VERSION = '0.3.1'
DESCRIPTION = """
CMS for Mercearia Digital
"""
LONG_DESCRIPTION = DESCRIPTION

setup(name='mdcms',
      version=VERSION,
      author='Juca Crispim',
      author_email='juca@poraodojuca.net',
      url='',
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      packages=find_packages(exclude=['tests', 'tests.*']),
      include_package_data=True,
      install_requires=['django>=1.7', 'django_merceariadigital>=0.2'],
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Web Environment',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Natural Language :: English',
          'Operating System :: OS Independent',
      ],
      test_suite='tests',
      provides=['mdcms'],)
